const availableCombinations = [
    'deen',
    'arde',
    'bgde',
    'dezh',
    'defr',
    'deel',
    'deit',
    'denl',
    'dept',
    'depl',
    'desl',
    'dees',
    'detr',
    'deru',
    'dela',
    'aren',
    'bgen',
    'enzh',
    'enfr',
    'enit',
    'enpl',
    'enpt',
    'enru',
    'ensl',
    'enes',
    'eszh',
    'esfr',
    'espl',
    'espt',
    'essl',
    'frzh',
    'frsl',
    'frpl',
    'itpl',
    'itsl',
    'plru'
];

browser.storage.local.get(["combination"]).then(configuration => {
    if (isEmpty(configuration)) {
        console.log('init')
        configuration = {
            combination: 'deen'
        };
        browser.storage.local.set(configuration);
    }
    return configuration;
}).then(configuration => {
    initialize(configuration);
});


function initialize(configuration) {
    console.log('configuration::', configuration);

    for (let i = 0; i < availableCombinations.length; i++) {
        let combination = availableCombinations[i];

        let option = document.createElement("div");

        let optionInput = document.createElement("input");
        optionInput.setAttribute("id", combination);
        optionInput.setAttribute("type", "radio");
        optionInput.setAttribute("name", "languages-options");
        optionInput.setAttribute("value", combination);
        if (configuration.combination == combination) {
            optionInput.checked = true;
        }
        optionInput.addEventListener("change", function () {
            let config = {
                combination: combination
            };
            browser.storage.local.set(config);
        });

        let optionLabel = document.createElement("label");
        optionLabel.setAttribute("for", combination);

        let langLeft = combination.substring(0, 2);
        let langRight = combination.substring(2, 4);
        optionLabel.innerHTML = browser.i18n.getMessage(langLeft) + " - " + browser.i18n.getMessage(langRight);


        option.appendChild(optionInput);
        option.appendChild(optionLabel);

        document.getElementById("availabe-options").appendChild(option);
    }
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}