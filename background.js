const translationTerm = {
  'en': 'translate',
  'es': 'traducción',
  'de': 'übersetzung',
  'fr': 'traduction',
  'it': 'traduzione',
  'tr': 'çeviri',
  'zh': '翻译',
  'pl': 'tłumaczenie',
  'pt': 'tradução',
  'ru': 'перевод'
};

const searchMenuId = "pons-selection"


// define global settings/properties
const ponsLanguage = browser.i18n.getUILanguage().toLowerCase().substring(0, 2);
const ponsTranslationTerm = translationTerm[ponsLanguage]
if (!ponsTranslationTerm) {
  ponsLanguage = 'en';
  ponsTranslationTerm = translationTerm['en'];
}


browser.menus.create({
  id: searchMenuId,
  title: 'Search at Pons',
  contexts: ["selection"]
}, onCreated);


function onCreated() {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Pons Lookup context menu icon added.");
  }
}

function onTranslate(item) {
  console.log(item);
  if (!selWord) {
    return
  }

  if (item.combination) {
    selDirection = item.combination;
  } else {
    selDirection = 'deen'
  }

  const urlString = `https://${ponsLanguage}.pons.com/${ponsTranslationTerm}?q=${selWord}&l=${selDirection}`;
  console.log(urlString)

  browser.tabs.create({
    active: true,
    url: urlString
  })
}

function onError(error) {
  console.log(`Error: ${error}`);
}

browser.menus.onClicked.addListener(function (info, tab) {
  if (info.menuItemId == searchMenuId) {
    selWord = info.selectionText.trim();
    browser.storage.local.get("combination").then(onTranslate, onError);
  }   
});


browser.commands.onCommand.addListener(command => {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.executeScript({ code: "window.getSelection().toString();" }, function (selection) {
      selWord = selection[0] || "";
      if (selWord) {
        browser.storage.local.get("combination").then(onTranslate, onError);
      }
    });
  });

})
