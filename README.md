<h1>Pons LookUp</h1>
<h2>A simple, but useful Firefox Add-on</h2>
<p>
Adds a context menu item to search for a selected word in the PONS online dictionary.
Clicking the menu item opens a new tab with the search results on pons.com.

Select text and press <code>Ctrl+Alt+T</code> for shortkey.
</p>


<h3>How to manually install</h3>
<p>

- Download latest version in <code>web-ext-artifacts</code> folder.

- Set <code>xpinstall.signatures.required=false</code> at Firefox <code>about:config</code> to be able to install unsigned addons.

- At Addons menu select Install From File and select the previously downloaded .zip file.
</p>